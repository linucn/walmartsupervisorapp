package com.walmart.supervisor.view;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.walmart.supervisor.R;
import com.walmart.supervisor.view.fragments.OrdersFragment;
import com.walmart.supervisor.view.fragments.PickedOrdersFragment;

public class OrderSectionsAdapter extends FragmentPagerAdapter {
    private static final int ORDERS = 0;
    private static final int PICKED_ORDERS = 1;

    private static final int[] TABS = new int[]{ORDERS, PICKED_ORDERS};

    private Context mContext;

    public OrderSectionsAdapter(final Context context, final FragmentManager fragmentManager) {
        super(fragmentManager);
        mContext = context.getApplicationContext();
    }

    @Override
    public Fragment getItem(int position) {
        switch (TABS[position]) {
            case ORDERS:
                return PickedOrdersFragment.getInstance(PICKED_ORDERS);
            case PICKED_ORDERS:
                return PickedOrdersFragment.getInstance(PICKED_ORDERS);
        }
        return null;
    }

    @Override
    public int getCount() {
        return TABS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (TABS[position]) {
            case ORDERS:
                return mContext.getResources().getString(R.string.orders_tab1_name);
            case PICKED_ORDERS:
                return mContext.getResources().getString(R.string.orders_tab2_name);
        }
        return null;
    }
}