/**
 * Copyright 2016 Erik Jhordan Rey. <p/> Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at <p/> http://www.apache.org/licenses/LICENSE-2.0 <p/> Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.order.PaymentDetails;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.NetworkUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class PickedOrderViewModel extends Observable {

    public ObservableInt orderProgress;
    public ObservableInt orderRecycler;
    public ObservableInt orderLabel;
    public ObservableField<String> messageLabel;

    private List<Orders> orderList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PickedOrderViewModel(@NonNull Context context) {
        this.context = context;
        this.orderList = new ArrayList<>();
        orderProgress = new ObservableInt(View.GONE);
        orderRecycler = new ObservableInt(View.GONE);
        orderLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_order));

        initializeViews();
        if (NetworkUtil.isOnline(context)) {
            fetchPickedOrderList();
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }
    }

    public void onClickFabLoad(View view) {

    }

    //It is "public" to show an example of test
    public void initializeViews() {
        orderLabel.set(View.VISIBLE);
        orderRecycler.set(View.GONE);
        orderProgress.set(View.VISIBLE);
    }

    public void fetchPickedOrderList() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("FromDate", "2018-06-18");
        aParams.put("ToDate", "2018-06-19");
        aParams.put("UserId", "");
        Logger.logInfo("fetchOrderHistory", "--- START fetchOrderHistory ", "");

        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/History").setMethod(APIMethod.POST).build();
        Disposable disposable = orderService.fetchOrderHistory(aDAPIHelper.getFinalUrl(), aParams)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        filterOrderList(orderResponse.getOrderList());
                        Logger.logInfo("fetchOrderHistory", "--- Order History Success MESSAGE CODE " + orderResponse.messageCode, "");
                        Logger.logInfo("fetchOrderHistory", "--- " + orderResponse.messageCode, "");
                        orderProgress.set(View.GONE);
                        orderLabel.set(View.GONE);
                        orderRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        messageLabel.set(context.getString(R.string.error_loading_order));
                        Logger.logInfo("fetchOrderHistory", "--- Error " + throwable.getMessage(), "");
                        throwable.printStackTrace();
                        orderProgress.set(View.GONE);
                        orderLabel.set(View.VISIBLE);
                        orderRecycler.set(View.GONE);
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void filterOrderList(List<Orders> ordersList) {
        Logger.logInfo("filterOrderList", "--- ordersList.size() " + ordersList.size(), "");
        List<Orders> filteredOrdersList = new ArrayList<>();
        for (Orders order : ordersList) {
            if (order.Status.equalsIgnoreCase("A")) {
                boolean isPaymentCredit = false;
                PaymentLoop:
                for (int i = 0; i < order.PaymentDetails.size(); i++) {
                    PaymentDetails paymentDetails = order.PaymentDetails.get(i);
                    Logger.logInfo("filterOrderList", "--- paymentDetails.PaymentTypes " + paymentDetails.PaymentType, "");
                    Logger.logInfo("filterOrderList", "--- paymentDetails.PaymentOption " + paymentDetails.PaymentOption, "");
                    if (paymentDetails.PaymentType.equalsIgnoreCase("TPG")) {
                        isPaymentCredit = true;
                        break PaymentLoop;
                    }
                }
                if (!isPaymentCredit)
                    filteredOrdersList.add(order);
            }
        }
        Logger.logInfo("filterOrderList", "--- filteredOrdersList.size() " + filteredOrdersList.size(), "");
        changeOrderDataSet(filteredOrdersList);
    }


    private void changeOrderDataSet(List<Orders> orders) {
        orderList.addAll(orders);
        setChanged();
        notifyObservers();
    }

    public List<Orders> getOrderList() {
        return orderList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}
