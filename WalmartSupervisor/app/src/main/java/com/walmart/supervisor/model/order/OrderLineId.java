package com.walmart.supervisor.model.order;

import java.io.Serializable;

public class OrderLineId implements Serializable {
    public int OrderLineId;
    public int OrderId;
    public int ProductId;
    public int VariantProductId;
    public boolean IsParentProduct;
    public String Description;
    public int Quantity;
    public float ShippingCost;
    public float ProductPrice;
    public float TotalPromotionDiscount;
    public String ReturnReason;
    public String ReturnAction;
    public String StockAction;
    public int ReturnQty;
    public boolean IsBackOrder;
    public float TotalVoucherDiscount;
    public float TotalTaxAmount;
    public float ShippingVoucherDiscount;
    public String CustomFields;
    public int VariantMasterProductId;
    public String LocationCode;
    public String ShippingStatus;
    public String DeliveryMode;
    public String VendorId;
    public String ItemStatus;
    public String SKU;
    public String VariantSku;
    public String ProductTitle;
    public String BundleProductId;
    public String ParentReDetailsId;
    public String ImageUrl;
    public String IsPrimaryProduct;
    public String Portion;
    public String MRP;
    public String CategoryName, CategoryId;

    public Product product;
}
