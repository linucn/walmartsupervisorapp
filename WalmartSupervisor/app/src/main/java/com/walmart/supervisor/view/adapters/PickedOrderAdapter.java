/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.ItemPickedOrderBinding;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.viewmodel.ItemOrderViewModel;

import java.util.Collections;
import java.util.List;

public class PickedOrderAdapter extends RecyclerView.Adapter<PickedOrderAdapter.PickedOrderAdapterViewHolder> {

    private List<Orders> orderList;

    public PickedOrderAdapter() {
        this.orderList = Collections.emptyList();
    }

    @Override
    public PickedOrderAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPickedOrderBinding itemPickedOrderBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_picked_order,
                        parent, false);
        return new PickedOrderAdapterViewHolder(itemPickedOrderBinding);
    }

    @Override
    public void onBindViewHolder(PickedOrderAdapterViewHolder holder, int position) {
        holder.bindOrder(orderList.get(position));
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public void setOrderList(List<Orders> orderList) {
        this.orderList = orderList;
        notifyDataSetChanged();
    }

    public static class PickedOrderAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemPickedOrderBinding itemPickedOrderBinding;

        public PickedOrderAdapterViewHolder(ItemPickedOrderBinding itemPickedOrderBinding) {
            super(itemPickedOrderBinding.itemOrder);
            this.itemPickedOrderBinding = itemPickedOrderBinding;
        }

        void bindOrder(Orders order) {
            Logger.logInfo("OrderAdapter", "--- order.Status " + order.Status, "  " + order.Status);
            if (itemPickedOrderBinding.getOrderViewModel() == null) {
                itemPickedOrderBinding.setOrderViewModel(
                        new ItemOrderViewModel(order, itemView.getContext()));
            } else {
                itemPickedOrderBinding.getOrderViewModel().setOrder(order);
            }
        }
    }
}
