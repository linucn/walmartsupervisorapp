package com.walmart.supervisor.model.order;

import java.io.Serializable;

public class PaymentDetails implements Serializable {
    public String PointsBurned;
    public String AgentId;
    public String GV;
    public String Channel;
    public String CurrencyCode;
    public String PaymentResponse;
    public String PaymentStatus;
    public String Amount;
    public String PaymentDate;
    public String PaymentType;
    public int OrderId;
    public int PaymentDetailsId;
    public String checkOutGroup;
    public String PaymentOption;
    public String clientIP;
    public String ResponseCode;
    public String ClientUserAgent;
}
