/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.walmart.supervisor.R;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.utils.Logger;


public class ItemOrderDetailViewModel extends BaseObservable {

    private OrderLineId orderLineId;
    private Context context;

    public ItemOrderDetailViewModel(OrderLineId orderLineId, Context context) {
        this.orderLineId = orderLineId;
        this.context = context;
    }

    public String getProductTitle() {
        if (orderLineId.ProductTitle != null) {
            return orderLineId.ProductTitle;
        }
        return context.getString(R.string.not_available);
    }

    public String getMRP() {
        if (orderLineId.MRP != null) {
            return "MRP : " + String.valueOf(orderLineId.MRP);
        }
        return "MRP : " + context.getString(R.string.not_available);
    }

    public String getQuantity() {
        return String.valueOf(orderLineId.Quantity);
    }

    public String getSKU() {
        return "Item Number : " + String.valueOf(orderLineId.SKU);
    }

    public String getProductPrice() {
        if (orderLineId.ProductPrice >= 0) {
            return "Current Price  : " + String.valueOf(orderLineId.ProductPrice);
        }
        return "Current Price  : " + context.getString(R.string.not_available);
    }

    public String getOrderedPrice() {
        if (orderLineId.ProductPrice >= 0) {
            return "Ordered Price  : " + String.valueOf(orderLineId.ProductPrice);
        }
        return "Ordered Price  : " + context.getString(R.string.not_available);
    }

    public String getProductImage() {
        Logger.logInfo("ItemOrderDetailViewModel", "getPictureProfile", "" + orderLineId.ImageUrl);
        String imageUrl = orderLineId.ImageUrl.split(";")[0];
        Logger.logInfo("ItemOrderDetailViewModel", "filtered getPictureProfile", "" + imageUrl);
        return imageUrl;
    }

    public String getTotalAmount() {
        return String.valueOf(orderLineId.ProductPrice);
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).placeholder(R.mipmap.ic_no_image).into(imageView);
    }

    public void setOrderLineId(OrderLineId orderLineId) {
        this.orderLineId = orderLineId;
        notifyChange();
    }
}
