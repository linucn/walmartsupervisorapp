package com.walmart.supervisor.model;

/**
 * Created by Srihari on 08/06/2018.
 */

public class LoginResponse {
    public String messageCode;
    public String Message;
    public AccessToken Token;

    @Override
    public String toString() {
        return " MessageCode "+messageCode+" Message "+Message;
    }
}
