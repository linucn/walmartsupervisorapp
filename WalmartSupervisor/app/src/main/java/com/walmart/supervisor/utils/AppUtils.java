package com.walmart.supervisor.utils;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by Srihari on 08/06/2018.
 */
public class AppUtils {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
