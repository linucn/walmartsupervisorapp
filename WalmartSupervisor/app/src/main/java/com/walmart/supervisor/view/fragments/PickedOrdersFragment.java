package com.walmart.supervisor.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.PickedOrderFragmentBinding;
import com.walmart.supervisor.view.adapters.PickedOrderAdapter;
import com.walmart.supervisor.viewmodel.OrderViewModel;
import com.walmart.supervisor.viewmodel.PickedOrderViewModel;

import java.util.Observable;
import java.util.Observer;

public class PickedOrdersFragment extends Fragment implements Observer {

    int position;
    PickedOrderFragmentBinding pickedOrderFragmentBinding;
    private PickedOrderViewModel pickedOrderViewModel;

    public static Fragment getInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        PickedOrdersFragment pickedOrdersFragment = new PickedOrdersFragment();
        pickedOrdersFragment.setArguments(bundle);
        return pickedOrdersFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.picked_order_fragment, container, false);
        pickedOrderFragmentBinding = PickedOrderFragmentBinding.bind(layout);
        initDataBinding();
        setupObserver(pickedOrderViewModel);
        setupListOrderView(pickedOrderFragmentBinding.listOrder);
        return layout;
    }

    private void setupListOrderView(RecyclerView listOrder) {
        PickedOrderAdapter adapter = new PickedOrderAdapter();
        listOrder.setAdapter(adapter);
        listOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initDataBinding() {
        pickedOrderViewModel = new PickedOrderViewModel(getActivity());
        pickedOrderFragmentBinding.setMainPickedOrderViewModel(pickedOrderViewModel);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof OrderViewModel) {
            PickedOrderAdapter pickedOrderAdapter = (PickedOrderAdapter) pickedOrderFragmentBinding.listOrder.getAdapter();
            PickedOrderViewModel pickedOrderViewModel = (PickedOrderViewModel) observable;
            pickedOrderAdapter.setOrderList(pickedOrderViewModel.getOrderList());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        pickedOrderViewModel.reset();
    }
}