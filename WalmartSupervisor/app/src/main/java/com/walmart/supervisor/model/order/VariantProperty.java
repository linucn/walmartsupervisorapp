package com.walmart.supervisor.model.order;


import java.io.Serializable;
import java.util.ArrayList;

public class VariantProperty implements Serializable {
    public int PropertyId;
    public int PropertyValueId;
    public String PropertyName;
    public String propertyvalue;
    public String propertyvaluedescription;
    public int sequence;
    public String swatchimage;
    public boolean isdisplayswatch;
    public int rank;
    public ArrayList<VariantProperty> VariantValues;

    @Override
    public boolean equals(Object other) {
        return !(other == null || other.getClass() != getClass()) && this.PropertyValueId == ((VariantProperty) other).PropertyValueId;
    }
}
