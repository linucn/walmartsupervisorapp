package com.walmart.supervisor.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Srihari on 08/06/2018.
 */

public class NetworkUtil {

    private static boolean mConnected = false;

    public static boolean isOnline(Context aContext) {
        try {
            ConnectivityManager aConnectivityManager = (ConnectivityManager) aContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo aNetworkInfo = aConnectivityManager.getActiveNetworkInfo();
            mConnected = aNetworkInfo != null && aNetworkInfo.isAvailable() && aNetworkInfo.isConnected();
            return mConnected;
        } catch (Exception e) {
            Logger.logDebug("NetworkUtil", "CheckConnectivity Exception: ", e.getMessage());
        }
        return mConnected;
    }
}
