package com.walmart.supervisor.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.LoginResponse;
import com.walmart.supervisor.utils.AppUtils;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.OrderActivity;

import java.util.Observable;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class LoginViewModel extends Observable {

    public ObservableInt loginProgress;
    public ObservableField<String> messageLabel;

    public final ObservableField<String> username;
    public final ObservableField<String> password;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LoginViewModel(@NonNull Context context) {
        this.context = context;
        loginProgress = new ObservableInt(View.GONE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_order));
        username = new ObservableField<>("vijay.vemana@walmart.com");
        password = new ObservableField<>("Walmart20!*");
    }

    public void onClickLogin(View view) {
        String usernameStr = username.get();
        String passwordStr = password.get();
        if (NetworkUtil.isOnline(context)) {
            if (isUserDataValid(usernameStr, passwordStr)) {
                doUserLogin(usernameStr, passwordStr);
                loginProgress.set(View.VISIBLE);
            }
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }
    }

    boolean isUserDataValid(String username, String password) {
        if (!AppUtils.isValidEmail(username)) {
            new SupervisorAppMessages().showStyleableToast(context, "Enter valid Username!", R.color.white, R.color.toast_bg);
            return false;
        }
        if (!(password.length() > 0)) {
            new SupervisorAppMessages().showStyleableToast(context, "Enter valid Password!", R.color.white, R.color.toast_bg);
            return false;
        }
        return true;
    }

    private void proceedToOrdersList() {
        ((Activity) context).finish();
        context.startActivity(OrderActivity.launchOrder(context));
    }

    public void doUserLogin(String username, String password) {

        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("customer").setServiceName("agentlogin").setMethod(APIMethod.POST).build();
        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        Disposable disposable = orderService.checkLoginValidity(aDAPIHelper.getFinalUrl(), username, password)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse loginResponse) throws Exception {
                        loginProgress.set(View.GONE);
                        if (loginResponse.messageCode.equalsIgnoreCase("1004")) {
                            proceedToOrdersList();
                            Toast.makeText(context, "Welcome!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, "" + loginResponse.Message, Toast.LENGTH_LONG).show();
                        }
                        Log.i("accept", "--- ResponseBody " + loginResponse.toString());
                        //Remove when you get Valid credentials
                        proceedToOrdersList();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        messageLabel.set(context.getString(R.string.error_loading_order));
                        Toast.makeText(context, "Failed - " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                        loginProgress.set(View.GONE);
                        Log.i("accept", "--- Throwable throwable " + throwable.getMessage() + "\n" + throwable.toString());
                        throwable.printStackTrace();
                    }
                });

        compositeDisposable.add(disposable);
    }


}
