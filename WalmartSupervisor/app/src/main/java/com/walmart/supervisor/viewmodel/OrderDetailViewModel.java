/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderFactory;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.bumptech.glide.Glide;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class OrderDetailViewModel extends Observable {

    private Orders Order;
    public ObservableInt orderProgress;
    public ObservableInt orderRecycler;
    public ObservableInt orderLabel;
    public ObservableField<String> messageLabel;

    String orderID;
    List<OrderLineId> orderLineIdList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OrderDetailViewModel(@NonNull Context context, Orders Order) {
        this.context = context;
        this.Order = Order;
        orderID = String.valueOf(Order.OrderId);
        this.orderLineIdList = new ArrayList<>();
        this.orderProgress = new ObservableInt(View.GONE);
        this.orderRecycler = new ObservableInt(View.GONE);
        this.orderLabel = new ObservableInt(View.GONE);
        this.messageLabel = new ObservableField<>(context.getString(R.string.default_loading_items));
        initializeViews();
        fetchOrderDetailList();
    }

    public void initializeViews() {
        orderLabel.set(View.VISIBLE);
        orderRecycler.set(View.GONE);
        orderProgress.set(View.VISIBLE);
    }

    public String getOrderId() {
        return String.valueOf(Order.OrderId);
    }

    public String getOrderDate() {
        if (Order != null && Order.OrderDate != null) {
            String[] aSplittedArray = Order.OrderDate.split("\\(");
            String aTime = aSplittedArray[1].split("\\+")[0];
            return DateHelper.getDate(aTime);
        }
        return "--";
    }

    public String getTotalAmount() {
        return String.valueOf(Order.TotalAmount);
    }

    public List<OrderLineId> getOrderList() {
        return orderLineIdList;
    }

    public void onClickAddNewItem(View view) {
        Toast.makeText(view.getContext(), "Not implemented!", Toast.LENGTH_LONG).show();
    }

    public void onClickRefresh(View view) {
        this.messageLabel.set(context.getString(R.string.default_refreshing_items));
        initializeViews();
        fetchOrderDetailList();
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    public void fetchOrderDetailList() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();

        if (!TextUtils.isEmpty(orderID)) {
            Logger.logInfo("fetchOrderDetail", "--- ORDER ID " + orderID, "");
            DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order").setServiceName(String.valueOf(orderID)).setMethod(APIMethod.GET).build();
            Disposable disposable = orderService.getOrderDetails(orderAPIHelper.getFinalUrl())
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<OrderResponse>() {
                        @Override
                        public void accept(OrderResponse orderResponse) throws Exception {
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.GONE);
                            orderRecycler.set(View.VISIBLE);
                            if (orderResponse.getOrderList().size() > 0) {
                                changeOrderDataSet(orderResponse.getOrderList().get(0).OrderLineId);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("fetchOrderDetail", "--- throwable", "");
                        }
                    });
            compositeDisposable.add(disposable);
        } else {
            Toast.makeText(context, "Error Occured!", Toast.LENGTH_LONG).show();
        }
    }

    private void changeOrderDataSet(List<OrderLineId> orderLineIdList) {
        this.orderLineIdList.clear();
        this.orderLineIdList.addAll(orderLineIdList);
        setChanged();
        notifyObservers();
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

}
