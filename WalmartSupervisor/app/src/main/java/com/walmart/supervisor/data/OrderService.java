/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.data;


import com.walmart.supervisor.model.LoginResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OrderService {

    @Headers("Accept: application/json")
    @POST
    @FormUrlEncoded
    Observable<LoginResponse> checkLoginValidity(@Url String url, @Field("username") String username, @Field("password") String password);

    @GET
    Observable<OrderResponse> fetchOrder(@Url String url);


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<OrderResponse> fetchOrderHistory(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Content-Type: application/json")
    @GET
    Observable<OrderResponse> getOrderDetails(@Url String url);


}
